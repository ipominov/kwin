# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2012, 2013, 2017.
# Vit Pelcak <vit@pelcak.org>, 2017, 2020, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-21 02:02+0000\n"
"PO-Revision-Date: 2023-06-05 13:11+0200\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 23.04.1\n"

#: module.cpp:50
#, kde-format
msgid "Import KWin Script"
msgstr "Importovat skript KWinu"

#: module.cpp:51
#, kde-format
msgid "*.kwinscript|KWin scripts (*.kwinscript)"
msgstr "*.kwinscript|Skripty KWinu (*.kwinscript)"

#: module.cpp:62
#, kde-format
msgctxt "Placeholder is error message returned from the install service"
msgid ""
"Cannot import selected script.\n"
"%1"
msgstr ""
"Nelze importovat zvolený skript.\n"
"%1"

#: module.cpp:66
#, kde-format
msgctxt "Placeholder is name of the script that was imported"
msgid "The script \"%1\" was successfully imported."
msgstr "Skript \"%1\" byl úspěšně importován."

#: module.cpp:125
#, kde-format
msgid "Error when uninstalling KWin Script: %1"
msgstr "Chyba odinstalování skriptu KWin: %1"

#: ui/main.qml:23
#, kde-format
msgid "Install from File…"
msgstr "Instalovat ze souboru…"

#: ui/main.qml:27
#, kde-format
msgctxt "@action:button get new KWin scripts"
msgid "Get New…"
msgstr "Získat nové…"

#: ui/main.qml:65
#, kde-format
msgctxt "@info:tooltip"
msgid "Delete…"
msgstr "Smazat…"
